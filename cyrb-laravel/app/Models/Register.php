<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    use HasFactory;
    public $table='registers';
    protected $fillable=[
    'consumer_no','name','address','phno','energy_charge','duty','fixed_charge','meter_rent','meterrent_stateGST',
    'meterrent_centralGST','tariff','purpose','billing_cycle','consumed_units','phase','email','password'];
    public $timestamps=false;
}
