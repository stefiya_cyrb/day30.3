@extends('admindashboard')
@section('content')
<!-- /*******************
*@function:registered user page view by admin
*******************/ -->

<h3 class="bg-primary text-white text-center ">USER VIEW</h3>
	<form method="post" action="">
	<table class="container-fluid table table-striped table-bordered table-hover">
	<tr>
     <td>Id</td>
     <td>Name</td>
     <td>Address</td>
     <td>Phone number</td>
     <td>Energy charge</td>
     <td>Duty</td>
     <td>fixed charge</td>
     <td>meter rent</td>
     <td>meter rent stateGST</td>
     <td>meter rent centralGST</td>
     <td>tariff</td>
     <td>purpose</td>
     <td>billing cycle</td>
     <td>consumed units</td>
     <td>phase</td>
     <td>Consumer No</td>
     <td>Email</td>
    </tr>
    
@foreach($user as $value)
<tr><td>{{$value['id']}}</td> 
    <td>{{$value['consumer_no']}}</td>
    <td>{{$value['name']}}</td>
    <td>{{$value['address']}}</td>
    <td>{{$value['phno']}}</td>
    <td>{{$value['energy_charge']}}</td>
    <td>{{$value['duty']}}</td>
    <td>{{$value['fixed_charge']}}</td>
    <td>{{$value['meter_rent']}}</td>
    <td>{{$value['meterrent_stateGST']}}</td>
    <td>{{$value['meterrent_centralGST']}}</td>
    <td>{{$value['tariff']}}</td>
    <td>{{$value['purpose']}}</td>
    <td>{{$value['billing_cycle']}}</td>
    <td>{{$value['consumed_units']}}</td>
    <td>{{$value['phase']}}</td>
    <td>{{$value['email']}}</td>
    

</tr>
@endforeach
</table>

	
</form>
@endsection