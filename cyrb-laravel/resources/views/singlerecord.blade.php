@extends('userdashboard')
@section('content')

<?php
/*******************
*@function:view single passenger record
*******************/
?>
<h1>SINGLE VIEW RECORD</h1>

  <form method="get" action="getview" class="container-fluid">
   
  <table>
    <tr>
      
    <td>Id</td>
     <td>Name</td>
     <td>Address</td>
     <td>Phone number</td>
     <td>Energy charge</td>
     <td>Duty</td>
     <td>fixed charge</td>
     <td>meter rent</td>
     <td>meter rent stateGST</td>
     <td>meter rent centralGST</td>
     <td>tariff</td>
     <td>purpose</td>
     <td>billing cycle</td>
     <td>consumed units</td>
     <td>phase</td>
     <td>Consumer No</td>
     <td>Total amount</td>
    
      
     
      
      </tr>
@foreach ($data as $user) 

    <td>{{$user['id']}}</td> 
    <td>{{$user['consumerno']}}</td>
    <td>{{$user['name']}}</td>
    <td>{{$user['address']}}</td>
    <td>{{$user['phno']}}</td>
    <td>{{$user['energy_charge']}}</td>
    <td>{{$user['duty']}}</td>
    <td>{{$user['fixed_charge']}}</td>
    <td>{{$user['meter_rent']}}</td>
    <td>{{$user['meterrent_stateGST']}}</td>
    <td>{{$user['meterrent_centralGST']}}</td>
    <td>{{$user['tariff']}}</td>
    <td>{{$user['purpose']}}</td>
    <td>{{$user['billing_cycle']}}</td>
    <td>{{$user['consumed_units']}}</td>
    <td>{{$user['phase']}}</td>
    <td>{{$user['email']}}</td>
    <td><a href="getview">View</a></td>




</tr>
@endforeach
  </table>
  
</form>
@endsection