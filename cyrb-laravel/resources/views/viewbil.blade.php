@extends('admindashboard')
@section('content')
<!-- /*******************
*@function:view bill
*******************/ -->

<h3 class="bg-primary text-white text-center ">Bill VIEW</h3>
	<form method="post" action="">
	<table class="container-fluid table table-striped table-bordered table-hover">
	<tr>
     
     <td>tariff</td>
     <td>purpose</td>
     <td>billing cycle</td>
     <td>consumed units</td>
     <td>phase</td>
     
    </tr>
    
@foreach($user as $value)
<tr>
    <td>{{$value['tariff']}}</td>
    <td>{{$value['purpose']}}</td>
    <td>{{$value['billing_cycle']}}</td>
    <td>{{$value['consumed_units']}}</td>
    <td>{{$value['phase']}}</td>
    

</tr>
@endforeach
</table>

	
</form>
@endsection