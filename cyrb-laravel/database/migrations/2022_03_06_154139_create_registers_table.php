<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->id();
            $table->integer('consumer_no');
            $table->string('name');
            $table->string('address');
            $table->integer('phno');
            $table->integer('energy_charge');
            $table->integer('duty');
            $table->integer('fixed_charge');
            $table->integer('meter_rent');
            $table->integer('meterrent_stateGST');
            $table->integer('meterrent_centralGST');
            $table->string('tariff');
            $table->string('purpose');
            $table->string('billing_cycle');
            $table->integer('consumed_units');
            $table->string('phase');
            $table->string('email');
            $table->string('password');
        
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
};
